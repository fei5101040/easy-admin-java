<!DOCTYPE html>
<html lang="zh" xmlns:th="http://www.thymeleaf.org">
<head>
    <link rel="stylesheet" th:href="@{/static/element/index.css}"/>
</head>
<body>
<style>
    .el-input-item {
        width: 260px;
    }

</style>
<div id="app">
    <template>
        <el-form :model="formData" ref="formData" :rules="rules" label-width="100px"
                 style="margin-top: 5%;display: flex;flex-direction: row;align-items: center;flex-wrap: wrap;justify-content: space-between;margin-right: 5%">
            #foreach($column in $columns)
                #set($field=$column.javaField)
                #if($column.insert && !$column.pk)
                    #if(($column.usableColumn) || (!$column.superColumn))
                        #set($parentheseIndex=$column.columnComment.indexOf("（"))
                        #if($parentheseIndex != -1)
                            #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                        #else
                            #set($comment=$column.columnComment)
                        #end
                        #set($dictType=$column.dictType)
                        #if($column.htmlType == "input")
                            <el-form-item label="${comment}" prop="${field}">
                                <el-input class="el-input-item" size="small" v-model="formData.${field}"
                                          placeholder="请输入${comment}"/>
                            </el-form-item>
                        #elseif($column.htmlType == "select" && "" != $dictType)
                            <el-form-item label="${comment}">
                                <el-select v-model="formData.${field}" placeholder="请选择${comment}" class="el-input-item"
                                           size="small">
                                    <el-option
                                            v-for="dict in ${field}Options"
                                            :key="dict.dictValue"
                                            :label="dict.dictLabel"
                                            #if($column.javaType == "Integer" || $column.javaType ==
                                                "Long"):value="parseInt(dict.dictValue)"#else:value="dict.dictValue"#end

                                    ></el-option>
                                </el-select>
                            </el-form-item>
                        #elseif($column.htmlType == "select" && $dictType)
                            <el-form-item label="${comment}">
                                <el-select v-model="formData.${field}" placeholder="请选择${comment}" class="el-input-item"
                                           size="small">
                                    <el-option label="请选择字典生成" value=""/>
                                </el-select>
                            </el-form-item>
                        #elseif($column.htmlType == "radio" && "" != $dictType)
                            <el-form-item label="${comment}">
                                <el-radio-group v-model="formData.${field}" class="el-input-item" size="small">
                                    <el-radio
                                            v-for="dict in ${field}Options"
                                            :key="dict.dictValue"
                                            #if($column.javaType == "Integer" || $column.javaType ==
                                                "Long"):label="parseInt(dict.dictValue)"#else:label="dict.dictValue"#end

                                    >{{dict.dictLabel}}
                                    </el-radio>
                                </el-radio-group>
                            </el-form-item>
                        #elseif($column.htmlType == "radio" && $dictType)
                            <el-form-item label="${comment}">
                                <el-radio-group v-model="formData.${field}" class="el-input-item" size="small">
                                    <el-radio label="1">请选择字典生成</el-radio>
                                </el-radio-group>
                            </el-form-item>
                        #elseif($column.htmlType == "datetime")
                            <el-form-item label="${comment}" prop="${field}">
                                <el-date-picker clearable size="small" style="width: 260px"
                                                v-model="formData.${field}"
                                                type="date"
                                                value-format="yyyy-MM-dd"
                                                placeholder="选择${comment}">
                                </el-date-picker>
                            </el-form-item>
                        #elseif($column.htmlType == "textarea")
                            <el-form-item label="${comment}" prop="${field}">
                                <el-input class="el-input-item" size="small" v-model="formData.${field}" type="textarea"
                                          placeholder="请输入内容"/>
                            </el-form-item>

                        #elseif($column.htmlType == "image")
                            <el-form-item label="${comment}" prop="${field}">
                                <div style="display: flex;flex-direction: row;align-items: center">
                                    <el-upload
                                            action="/common/upload/file"
                                            :on-success="handleSuccess${field}"
                                            list-type="picture-card"
                                            :on-error="handleError${field}"
                                            :limit="3"
                                            :file-list="default${field}List"
                                            :before-upload="beforeUpload${field}"
                                            style="margin-left: 10px"
                                    >
                                        <i class="el-icon-plus"></i>
                                    </el-upload>
                                </div>

                            </el-form-item>
                        #end
                    #end
                #end
            #end
        </el-form>
        <div style="text-align: center;margin-top: 5%">
            <el-button size="small" @click="closePage">取 消</el-button>
            <el-button size="small" type="primary" @click="handleSubmit">确 定</el-button>
        </div>
    </template>
</div>
</body>
<script th:src="@{/static/js/axios.min.js}"></script>
<script th:src="@{/static/js/request.js}"></script>
<script th:src="@{/static/js/common.js}"></script>
<script th:src="@{/static/api/${moduleName}/${jsName}.js}"></script>
<script th:src="@{/static/js/vue.min.js}"></script>
<script th:src="@{/static/element/index.js}"></script>
<script th:src="@{/static/js/jquery.min.js}"></script>
<script th:src="@{/static/layer/layer.js}"></script>
<script>
    let id = getQueryString("id");
    new Vue({
        el: '#app',
        data() {
            return {
                dialogVisible: false,
                formData: {},
                #foreach ($column in $columns)
                    #set($parentheseIndex=$column.columnComment.indexOf("（"))
                    #if($parentheseIndex != -1)
                        #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                    #else
                        #set($comment=$column.columnComment)
                    #end
                    #if(${column.htmlType} == 'select')
                        // $comment字典
                            ${column.javaField}Options: [],
                    #end
                    #if(${column.htmlType} == 'image')
                        default${column.javaField}List:[],
                    #end
                #end
                rules: {
                    #foreach ($column in $columns)
                        #if($column.required)
                            #set($parentheseIndex=$column.columnComment.indexOf("（"))
                            #if($parentheseIndex != -1)
                                #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                            #else
                                #set($comment=$column.columnComment)
                            #end
                            #set($comment=$column.columnComment)
                                $column.javaField: [
                                {required: true, message: "$comment不能为空", trigger: "blur"}
                            ]#if($velocityCount != $columns.size()),#end

                        #end
                    #end
                }
            }
        },
        created() {
            if (id) {
                detail(id).then((res) => {
                    if (res.code === '200') {
                        this.formData = res.data
                        #foreach ($column in $columns)
                            #if(${column.htmlType} == 'image')
                                let ${column.javaField}Arr=[]
                                    ${column.javaField}Arr.push(res.data.${column.javaField})
                                this.default${column.javaField}List =${column.javaField}Arr.map(item=>{
                                    return {
                                        name: item,
                                        url: item
                                    }
                                });
                            #end
                        #end
                    }
                });
            } else {
                this.formData = {}
            }
            #foreach ($column in $columns)
                #if(${column.htmlType} == 'select')
                    getDicts("${column.dictType}").then(response => {
                        this.${column.javaField}Options = response.data;
                    });
                #end
            #end

        },
        methods: {


            handleSubmit() {
                if (id) {
                    addOrUpdate('/admin/${jsName}/update', this);
                } else {
                    addOrUpdate('/admin/${jsName}/add', this);
                }
            },

            #foreach ($column in $columns)
                #if(${column.htmlType} == 'image')
                    handleSuccess${column.javaField}(response, file, fileList) {
                        console.log('上传成功', response, file, fileList);
                        if(response.code==='200'){
                            this.formData.picture = response.data
                        }else{
                            this.$message.error('上传失败');
                        }
                    },
                    handleError${column.javaField}(err, file, fileList) {
                        // 文件上传失败的回调函数
                        console.log('上传失败', err, file, fileList);
                    },
                    beforeUpload${column.javaField}(file) {
                        // 在文件上传之前的钩子函数，可以在这里进行文件格式、大小等的校验
                        const isJPG = file.type === 'image/jpeg' || file.type === 'image/png';
                        const isLt500KB = file.size / 1024 <= 500;
                        if (!isJPG) {
                            this.$message.error('只能上传jpg/png文件!');
                        }
                        if (!isLt500KB) {
                            this.$message.error('文件大小不能超过500KB!');
                        }
                        return isJPG && isLt500KB;
                    },
                #end
            #end
        }
    });
</script>
</html>
