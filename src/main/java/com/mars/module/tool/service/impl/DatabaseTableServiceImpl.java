package com.mars.module.tool.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.request.tool.TableQueryRequest;
import com.mars.common.response.PageInfo;
import com.mars.common.response.tool.TableListResponse;
import com.mars.module.tool.mapper.GenTableMapper;
import com.mars.module.tool.service.IDatabaseTableService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据库表Service
 *
 * @author 源码字节-程序员Mars
 */
@Service
@AllArgsConstructor
public class DatabaseTableServiceImpl implements IDatabaseTableService {

    private final GenTableMapper genTableMapper;

    /**
     * 系统内置表
     */
    public static final List<String> SYS_TABLE = new ArrayList<>();

    static {
        SYS_TABLE.add("sys_menu");
        SYS_TABLE.add("sys_role");
        SYS_TABLE.add("sys_role_menu");
        SYS_TABLE.add("sys_user");
        SYS_TABLE.add("sys_user_post");
        SYS_TABLE.add("sys_post");
        SYS_TABLE.add("sys_dict_data");
        SYS_TABLE.add("sys_dict_type");
        SYS_TABLE.add("sys_login_record");
        SYS_TABLE.add("sys_oper_log");
        SYS_TABLE.add("sys_user_role");
        SYS_TABLE.add("sys_config");
        SYS_TABLE.add("sys_oss");
    }


    @Override
    public PageInfo<TableListResponse> list(TableQueryRequest request) {
        Page<TableListResponse> page = new Page<>(request.getPageNo(), request.getPageSize());
        request.setSysTableList(SYS_TABLE);
        IPage<TableListResponse> pageList = genTableMapper.tableList(page, request);
        return PageInfo.build(pageList);
    }
}
