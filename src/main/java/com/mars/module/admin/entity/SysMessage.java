package com.mars.module.admin.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.*;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.*;
import cn.afterturn.easypoi.excel.annotation.Excel;

import java.time.LocalDateTime;

import com.mars.module.system.entity.BaseEntity;

/**
 * 消息对象 sys_message
 *
 * @author mars
 * @date 2023-12-06
 */

@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "消息对象")
@Builder
@Accessors(chain = true)
@TableName("sys_message")
public class SysMessage extends BaseEntity {


    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 发送方
     */
    @Excel(name = "发送方")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "发送方")
    private Long sender;

    /**
     * 消息发送方名称
     */
    private String senderName;

    /**
     * 接收方
     */
    @Excel(name = "接收方")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "接收方")
    private Long receiver;

    /**
     * 内容
     */
    @Excel(name = "内容")
    @ApiModelProperty(value = "内容")
    private String content;

    /**
     * 标题
     */
    @Excel(name = "标题")
    @ApiModelProperty(value = "标题")
    private String title;

    /**
     * 消息状态
     */
    @Excel(name = "消息状态")
    @ApiModelProperty(value = "消息状态")
    private Integer status;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 30, databaseFormat = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT, value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    /**
     * 创建人名称
     */
    @TableField(fill = FieldFill.INSERT, value = "create_by_name")
    @ApiModelProperty(value = "创建人名称")
    private String createByName;

    /**
     * 修改时间
     */
    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.UPDATE, value = "update_time")
    private LocalDateTime updateTime;

    /**
     * 更新人名称
     */
    @TableField(fill = FieldFill.UPDATE, value = "update_by_name")
    @ApiModelProperty(value = "更新人名称")
    private String updateByName;
}
