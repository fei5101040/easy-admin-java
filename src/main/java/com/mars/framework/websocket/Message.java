package com.mars.framework.websocket;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-12-06 15:00:28
 */
@Data
@Builder
@Accessors(chain = true)
public class Message {

    /**
     * 消息内容
     */
    private String content;

    /**
     * 消息数量
     */
    private Integer msgNumber;

    /**
     * 消息发送方ID
     */
    private Long senderId;

    /**
     * 消息接收方ID
     */
    private Long receiverId;
}
