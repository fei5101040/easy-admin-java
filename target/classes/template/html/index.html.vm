<!DOCTYPE html>
<html lang="zh" xmlns:th="http://www.thymeleaf.org">
<head>
    <link rel="stylesheet" th:href="@{/static/element/index.css}"/>
</head>
<body>
<div id="app">
    <template>
        <el-card style="background-color: #F5F7FA">
            <el-form :model="queryForm" ref="queryForm" :inline="true" label-width="100px" @submit.native.prevent>
                #foreach($column in $columns)
                    #if($column.query)
                        #set($dictType=$column.dictType)
                        #set($AttrName=$column.javaField.substring(0,1).toUpperCase() + ${column.javaField.substring(1)})
                        #set($parentheseIndex=$column.columnComment.indexOf("（"))
                        #if($parentheseIndex != -1)
                            #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                        #else
                            #set($comment=$column.columnComment)
                        #end
                        #if($column.htmlType == "input")
                            <el-form-item label="${comment}" prop="${column.javaField}">
                                <el-input
                                        v-model="queryForm.${column.javaField}"
                                        placeholder="请输入${comment}"
                                        clearable
                                        size="small"
                                        @keyup.enter.native="handleQuery"
                                />
                            </el-form-item>
                        #elseif(($column.htmlType == "select" || $column.htmlType == "radio") && "" != $dictType)
                            <el-form-item label="${comment}" prop="${column.javaField}">
                                <el-select v-model="queryForm.${column.javaField}" placeholder="请选择${comment}" clearable
                                           size="small">
                                    <el-option
                                            v-for="dict in ${column.javaField}Options"
                                            :key="dict.dictValue"
                                            :label="dict.dictLabel"
                                            :value="dict.dictValue"
                                    />
                                </el-select>
                            </el-form-item>
                        #elseif(($column.htmlType == "select" || $column.htmlType == "radio") && $dictType)
                            <el-form-item label="${comment}" prop="${column.javaField}">
                                <el-select v-model="queryForm.${column.javaField}" placeholder="请选择${comment}" clearable
                                           size="small">
                                    <el-option label="请选择字典生成" value=""/>
                                </el-select>
                            </el-form-item>
                        #elseif($column.htmlType == "datetime")
                            <el-form-item label="${comment}" prop="${column.javaField}">
                                <el-date-picker clearable size="small" style="width: 200px"
                                                v-model="queryForm.${column.javaField}"
                                                type="date"
                                                value-format="yyyy-MM-dd"
                                                placeholder="选择${comment}">
                                </el-date-picker>
                            </el-form-item>
                        #end
                    #end
                #end

                <el-form-item>
                    <el-button type="primary" icon="el-icon-search" size="mini" @click="handleQuery">搜索</el-button>
                    <el-button icon="el-icon-refresh" size="mini" @click="resetQuery">清空</el-button>
                </el-form-item>
            </el-form>
        </el-card>

        <el-row style="margin-top: 1%">
            <el-col :span="1.5">
                <el-button
                        type="primary"
                        icon="el-icon-plus"
                        size="mini"
                        @click="handleAddOrUpdate(1,null)"
                        v-if="button.add"
                >
                    新增
                </el-button>
                <el-button v-if="button.delete" size="mini" type="info" icon="el-icon-delete"
                           @click="batchHandleDelete">删除
                </el-button>
            </el-col>
        </el-row>


        <el-table
                v-loading="loading"
                :data="tableData" row-key="id"
                :header-cell-style="{'text-align':'center','color':'#303133','font-weight':'600','background-color': '#F5F7FA'}"
                :cell-style="{'text-align':'center'}" border style="margin-top: 10px"
                @selection-change="handleSelectionChange"
        >
            <el-table-column type="selection" align="center"></el-table-column>
            #foreach($column in $columns)
                #set($javaField=$column.javaField)
                #set($parentheseIndex=$column.columnComment.indexOf("（"))
                #if($parentheseIndex != -1)
                    #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                #else
                    #set($comment=$column.columnComment)
                #end
                #if($column.pk)
                    <el-table-column label="${comment}" align="center" prop="${javaField}"></el-table-column>
                #elseif($column.list && $column.htmlType == "datetime")
                    <el-table-column label="${comment}" align="center" prop="${javaField}" show-overflow-tooltip="true">
                        <template slot-scope="scope">
                            <span>{{ scope.row.${javaField}}}</span>
                        </template>
                    </el-table-column>
                #elseif($column.list && $column.htmlType == "image")
                    <el-table-column label="${comment}" align="center" prop="${javaField}">
                        <template slot-scope="scope">
                            <el-image
                                    style="width: 100px; height: 48px"
                                    :src="scope.row.${javaField}"
                                    fit="cover"
                                    :preview-src-list="preview${column.javaField}List"
                                    title="点击查看图片"
                                    @click="preview${column.javaField}(scope.row)"
                            ></el-image>
                        </template>
                    </el-table-column>
                #elseif($column.list && $column.htmlType == "select")
                    <el-table-column label="${comment}" align="center" prop="${javaField}"
                                     :formatter="${javaField}Format"></el-table-column>
                #elseif($column.list && $column.htmlType == "switch")
                    <el-table-column label="${comment}" align="center" prop="${javaField}">
                        <template slot-scope="scope">
                            <el-switch v-model="scope.row.${javaField}"
                                       :active-value="0"
                                       :inactive-value="1"
                                       active-text="正常"
                                       inactive-text="停用"
                                       @change="switchChange(scope.row)"></el-switch>
                        </template>
                    </el-table-column>
                #elseif($column.list && "" != $javaField)
                    <el-table-column label="${comment}" align="center" prop="${javaField}"></el-table-column>
                #end
            #end
            <el-table-column label="操作" width="200">
                <template slot-scope="scope">
                    <el-button v-if="button.update"  size="mini" type="primary" icon="el-icon-edit"
                               @click="handleAddOrUpdate(2,scope.row)">修改
                    </el-button>

                    <el-button v-if="button.delete" size="mini" type="info" icon="el-icon-delete" @click="handleDelete(scope.row)">删除
                    </el-button>
                </template>
            </el-table-column>
        </el-table>
        <el-pagination
                background
                :page-sizes="[10, 20, 30, 40,50]"
                layout="total, sizes, prev, pager, next, jumper"
                :total="total"
                :page-size.sync="pageSize"
                :current-page.sync="currentPage"
                @size-change="handleQuery"
                @current-change="handleQuery"
                style="float: right;margin-top: 10px"
        />
    </template>
</div>
</body>
<script th:src="@{/static/js/axios.min.js}"></script>
<script th:src="@{/static/js/request.js}"></script>
<script th:src="@{/static/js/common.js}"></script>
<script th:src="@{/static/api/${moduleName}/${jsName}.js}"></script>
<script th:src="@{/static/js/vue.min.js}"></script>
<script th:src="@{/static/element/index.js}"></script>
<script th:src="@{/static/js/jquery.min.js}"></script>
<script th:src="@{/static/layer/layer.js}"></script>
<script>
    new Vue({
        el: '#app',
        data() {
            return {
                currentPage: 1,
                pageSize: 10,
                total: 0,
                tableData: [],
                multipleSelection: [],
#foreach ($column in $columns)
#set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
        #set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
        #set($comment=$column.columnComment)
#end
#if($column.htmlType == "select")
    // $comment字典
        ${column.javaField}Options: [],
#end
#if($column.htmlType == "image")
    preview${column.javaField}List: [],
#end
#end
                button: {
                    add: false,
                    update: false,
                    delete: false
                },
                loading: false,
                queryForm: {
                    roleName: ''
                }
            }
        },
        created() {
            this.handleQuery();
            let sysButtons = JSON.parse(localStorage.getItem("mars-sysButtons"));
            this.button.add = sysButtons.indexOf("${moduleName}/${jsName}/add") > -1;
            this.button.update = sysButtons.indexOf("${moduleName}/${jsName}/update") > -1;
            this.button.delete = sysButtons.indexOf("${moduleName}/${jsName}/delete") > -1;
            #foreach ($column in $columns)
                #if($column.htmlType == "select")
                    getDicts("${column.dictType}").then(response => {
                        this.${column.javaField}Options = response.data;
                    });
                #end
            #end

        },
        methods: {
            handleSelectionChange(val) {
                this.multipleSelection = val;
            },
            batchHandleDelete(){
                let params = {}
                if (this.multipleSelection.length === 0) {
                    this.$notify({
                        title: '失败',
                        message: '请选择删除行',
                        duration: 2000
                    });
                    return
                }
                deleteById('/${moduleName}/${jsName}/delete/' + this.multipleSelection.map((item) => item.id), this);
            },
            switchChange(row) {
                this.formData = row
                update(this.formData).then(res => {
                    if (res.code === '200') {
                        this.$notify({
                            title: '成功',
                            message: '操作成功',
                            type: 'success'
                        });
                    }
                })
            },

#foreach ($column in $columns)
#if($column.htmlType == "select")
   #set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
    #set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
    #set($comment=$column.columnComment)
#end
            ${column.javaField}Format(row, column) {
                   return selectDictLabel(this.${column.javaField}Options, row.${column.javaField});
            },
#end
#end
            resetQuery() {
                this.queryForm = {}
                this.handleQuery()
            },

#foreach ($column in $columns)
#set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
    #set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
    #set($comment=$column.columnComment)
#end

#if($column.htmlType == "image")
           // 点击图片，大图预览多张图片
           preview${column.javaField} (row) {
                var srclist = []
                if (row.${column.javaField}) {
                   srclist.push(row.${column.javaField})
                   this.preview${column.javaField}List = srclist
                }
           },
#end
#end
            handleCurrentChange(val) {
                this.currentPage = val;
                this.handleQuery();
            },
            handleSizeChange(val) {
                this.pageSize = val;
                this.handleQuery();
            },
            handleQuery() {
                let queryParam = this.queryForm
                queryParam.pageNo = this.currentPage
                queryParam.pageSize = this.pageSize
                this.loading = true
                pageList(queryParam).then((res) => {
                    if (res.code === '200') {
                        this.tableData = res.data.list;
                        this.total = res.data.count;
                        this.loading = false
                    } else {
                        this.loading = false;
                        this.$notify({
                            title: '失败',
                            message: res.message,
                            type: 'error'
                        });
                    }
                });
            },
            handleDelete(row) {
                deleteById('/${moduleName}/${jsName}/delete/' + row.id, this);
            },

            handleAddOrUpdate(type, row) {
                if (type === 1) {
                    openPage('新增${functionName}', '/${moduleName}/${jsName}/${jsName}AddUpdate.html', this);
                } else {
                    openPage('修改${functionName}', '/${moduleName}/${jsName}/${jsName}AddUpdate.html?id=' + row.id, this);
                }
            },
        }
    });
</script>
</html>
